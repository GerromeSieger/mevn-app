FROM node:16.15.1-alpine

WORKDIR /app

COPY package.json .

RUN yarn install

COPY . .

#RUN npm install express-validator@5.3.0 --save-exact

EXPOSE 3000

